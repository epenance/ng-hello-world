FROM node

RUN npm install -g pm2

WORKDIR /var/www/app

COPY dist dist

EXPOSE 4000

CMD [ "pm2", "start", "--no-daemon", "dist/server.js"]

